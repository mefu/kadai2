#!/usr/bin/python2.7

import sys
import getopt
import json
import os
import itertools
import tempfile
import subprocess
import sqlite3
import logging
import shutil
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import LineCollection
from matplotlib.lines import Line2D
from simrun import SimRun
from nslib import NSLib, NSToSQLConverter

def main(argv):
  # disable byte code writing
  sys.dont_write_bytecode = True

  # create a logger
  logger = getLogger()

  # parse command line arguments
  config_file = parsein(argv)

  with open(config_file) as cf:
    # parse config file as json
    conf = json.load(cf)
    
    # base directory where config file is
    base_dir = os.path.dirname(os.path.realpath(cf.name))

    # create ns lib object that will be used to run ns commands
    nsl = NSLib(conf["nsbin"])

    # ns input string
    nsinput = conf["input"]["path"]
    if not nsinput.startswith("/"):
      nsinput = os.path.realpath(base_dir + "/" + nsinput)

    # parameter value to be used
    parameters = conf["input"]["parameters"]

    # read queries for sql
    queries = conf["queries_sql"]

    # read output format
    output_format = conf["output"]["format"]

    # run and get outputs
    datas = run(nsl, open(nsinput), parameters, queries, logger)

    # plot graphs if specified
    if "graph" in conf["output"]:
      plot(datas, conf["output"]["graph"], base_dir)

    # turn data to printable output lines
    outputs = prepareOutput(datas, output_format)
    
    # if output file specified
    if "file" in conf["output"]:
      output_file_path = conf["output"]["file"]
      if not output_file_path.startswith("/"):
        output_file_path = os.path.realpath(base_dir + "/" + output_file_path)
      with open(output_file_path, 'w') as of:
        of.write("\n".join(outputs))
    # else just write to console
    else:
      for line in outputs:
        print(line)

def run(nsl, nsinput, parameters, queries, logger):
  # aggregate output here
  datas = []

  # create simulate runner
  simrun = SimRun(simSQL, parameters)

  # run simulation
  results = simrun.run([nsl, nsinput, queries, logger])

  for (params, result) in results:
    data = mergeParamsOutput(result, dict(params))
    datas.extend(data)

  return datas

def simSQL(nsl, nsinput, queries, logger, *args):
  # temporary dir for for ns output files to live
  dir = tempfile.mkdtemp()
  #tf = tempfile.NamedTemporaryFile()

  logger.debug("Temporary directory " + dir + " created.")

  #conn = sqlite3.connect(tf.name)
  conn = sqlite3.connect(":memory:")

  logger.info("Running simulation with " + str(args) + ".")

  nsl.runToSQL(conn, nsinput, list(args), dir)

  logger.debug("Running queries.")

  ress = querySQLConn(conn, queries)

  logger.debug("Cleaning.")

  #raw_input(tf.name)

  conn.close()
  shutil.rmtree(dir)  # delete directory

  return ress
  
def querySQLConn(conn, queries):
  c = conn.cursor()

  ress = {}
  ress["ml"] = {}
  ress["sl"] = {}
  for qinfo in queries:
    res = c.execute(qinfo["query"]).fetchall()
    if len(res) > 1:
      ress["ml"][tuple(qinfo["name"])] = res
    elif len(res) == 1:
      ress["sl"][qinfo["name"]] = res[0][0]
  return ress

def mergeParamsOutput(ress, desc):
  merged = {}

  for name, val in desc.iteritems():
    merged[name] = val

  data = [ merged ]

  for names, vals in ress["ml"].iteritems():
    tmpdata = []
    for d in data:
      for val in vals:
        dtmp = d.copy()
        ls = zip(names, val)
        for (name, vall) in ls:
          dtmp[name] = vall
        tmpdata.append(dtmp)
    data = tmpdata

  for name, val in ress["sl"].iteritems():
    tmpdata = []
    for d in data:
      dtmp = d.copy()
      dtmp[name] = val
      tmpdata.append(dtmp)
    data = tmpdata

  return data

def prepareOutput(data, output_format):
  outputs = []
  for d in data:
    output_tmp = output_format
    for name, val in d.iteritems():
      output_tmp = output_tmp.replace("{"+name+"}", str(val).strip())
    outputs.append(output_tmp)
  return outputs

def plot(datas, confs, base_dir):
  for conf in confs:
    fig = plt.figure()

    ax = None
    if conf["type"] == "2d":
      ax = fig.add_subplot(111)
    elif conf["type"] == "3d":
      ax = fig.add_subplot(111, projection="3d")

    xarray = [data[conf["x"]["name"]]
              for data in datas]
    
    yarray = [data[conf["y"]["name"]]
              for data in datas]

    zarray = []
    if "z" in conf:
      zarray = [data[conf["z"]["name"]]
                for data in datas]

    lgd = None
    if not "z" in conf:
      ax.plot(xarray, yarray)
    else:
      xycollections = []
      (xmax, xmin) = (max(xarray), min(xarray))
      (ymax, ymin) = (max(yarray), min(yarray))
      (zmax, zmin) = (max(zarray), min(zarray))
      zset = sorted(list(set(zarray)))
      for z in zset:
        xarray = [data[conf["x"]["name"]]
                for data in datas
                if data[conf["z"]["name"]] == z]

        yarray = [data[conf["y"]["name"]]
                for data in datas
                if data[conf["z"]["name"]] == z]

        xycollections.append(list(zip(xarray, yarray)))

      lc = LineCollection(xycollections, array=np.array(zset), cmap = plt.get_cmap('rainbow'))

      if conf["type"] == "3d":
        ax.add_collection3d(lc, zs = zset)
        ax.set_xlim3d(xmin, xmax)
        ax.set_ylim3d(ymin, ymax)
        ax.set_zlim3d(zmin, zmax)
      elif conf["type"] == "2d":
        lines = ax.add_collection(lc)
        hndls = []
        base_label = "z = "
        if "label" in conf["z"]:
          base_label = conf["z"]["label"] + " "
        for idx, val in enumerate(xycollections):
          color = lines.cmap(lines.norm(lc.get_array())[idx])
          hndls.append(Line2D([], [], color = color, label=base_label + str(lc.get_array()[idx])))
        lgd = plt.legend(handles=hndls,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        ax.autoscale()

    if "title" in conf:
      ax.set_title(conf["title"])

    if "label" in conf["x"]:
      ax.set_xlabel(conf["x"]["label"])

    if "label" in conf["y"]:
      ax.set_ylabel(conf["y"]["label"])

    if conf["type"] == "3d" and "label" in conf["z"]:
      ax.set_zlabel(conf["z"]["label"])


    output_file_path = conf["file"]
    if not output_file_path.startswith("/"):
      output_file_path = os.path.realpath(base_dir + "/" + output_file_path)

    if "z" in conf and conf["type"] == "2d":
      plt.savefig(output_file_path, bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
      plt.savefig(output_file_path)

def getLogger():
  # setting up logging
  logger = logging.getLogger("simrun")
  logger.setLevel(logging.DEBUG)

  # create file handler which logs even debug messages
  fh = logging.FileHandler('simrun.log')
  fh.setLevel(logging.DEBUG)

  # create console handler with a higher log level
  ch = logging.StreamHandler()
  ch.setLevel(logging.INFO)

  # create formatter and add it to the handlers
  formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  fh.setFormatter(formatter)
  ch.setFormatter(formatter)

  # add the handlers to the logger
  logger.addHandler(fh)
  logger.addHandler(ch)

  return logger

def parsein(argv):
  configfile = ''
  helpstr = 'simrun.py -c <configfile>'
  try:
    opts, args = getopt.getopt(argv,"hc:",["help","config="])
  except getopt.GetoptError:
    print helpstr
    sys.exit(2)
  if not opts:
    print helpstr
    sys.exit(2)
  for opt, arg in opts:
    if opt in ("-h", "--help"):
      print helpstr
      sys.exit()
    elif opt in ("-c", "--config"):
      configfile = arg
    return configfile


if __name__ == "__main__":
  main(sys.argv[1:])
