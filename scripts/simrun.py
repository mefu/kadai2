#!/usr/bin/python2.7

import sys
import itertools

class SimRun(object):
	"""A simulation runner class."""
	def __init__(self, sim, parameters):
		self.sim = sim
		self.parameters = parameters

	def run(self, funcparams = []):
		"""Runs sim function with given parameters
		Iterates over parameter ranges and indexes
		results with parameters used for that run.
		parameters -- An array of parameter dicts
		parameter_dict -- name, range, suffix, type
		"""
		base = self.getBase()
		sim_params = self.populateBase(base)
		results = []
		for sim_param, sim_param_desc in sim_params:
			result = self.sim(*(funcparams + sim_param.strip().split(" ")))
			results.append((sim_param_desc, result))
		return results

	def getBase(self):
		"""Creates a base string for parameters
		Example: {bw}Mb {d} {qs}
		"""
		base = ""
		for par in self.parameters:
			base += " {" + par["name"] + "}"
			if "suffix" in par:
				base += par["suffix"]
		return base

	def populateBase(self, base):
		"""Populates base string with values
		of parameters. For {bw}Mb {d} {qs} an example
		result might be:
		16Mb 5 32
		16Mb 5 64
		16Mb 5 96
		"""
		sim_params = []

		par_vals = []
		par_names = []
		for par in self.parameters:
			par_names.append(par["name"])
			val = par["value"]
			if len(val) == 1:
				par_vals.append(val)
			elif len(val) == 3:
				par_vals.append(range(val[0],val[1]+val[2],val[2]))

		for parameters in itertools.product(*par_vals):
			tmp_param = base
			tmp_param_desc = {}
			for j, val in enumerate(parameters):
				tmp_param = tmp_param.replace("{"+par_names[j]+"}", str(val))
				tmp_param_desc[par_names[j]] = val

			sim_params.append((tmp_param, tmp_param_desc))

		return sim_params


if __name__ == "__main__":
  main(sys.argv[1:])

