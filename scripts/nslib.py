#!/usr/bin/python2.7

import sys
import os
import tempfile
import logging
import sqlite3
import glob
import sh

from abc import ABCMeta, abstractmethod

class NSLib(object):
	def __init__(self, nsbin):
		"""Initialize nslib with path to nsbin"""
		try:
			self.nsbin = getattr(sh, nsbin)
		except sh.CommandNotFound as cnf:
			raise NSLibError("Provided ns bin command " + nsbin + " could not be found")

		self.checkNSBin()

	def checkNSBin(self):
		"""Checks if nsbin works correctly
		This library requires ns version 2.35		
		"""
		with tempfile.NamedTemporaryFile() as test_file:
			# Create a test ns tcl file that only writes version
			test_file.write("puts [ns-version]")
			test_file.flush()

			# Create a command to run
			test_result = self.run(test_file, []) 
			if test_result != "2.35":
				raise NSLibError("Provided ns binary did not result in correct version. Current version = " + test_result)
			else:
				logging.getLogger("simrun").info("NS version is correct and binary is usable")

	def run(self, input_file, parameters, dir = False):
		"""Runs following command
		[nsbin] [input_file] [parameters]
		and returns command line output.
		dir -- Directory to run ns command in. Defaults to a newly created temp directory
		return_result -- If this function should return stdout of ns command
		"""
		if dir:
			sh.cd(dir)
		input_file_path = os.path.realpath(input_file.name)
		res = ""
		if not parameters:
			res = self.nsbin(input_file_path)
		else:
			res = self.nsbin(input_file_path, parameters)
		return res.strip()

	def runToSQL(self, conn, input_file, parameters, dir):
		result = self.run(input_file, parameters, dir)
		NSToSQLConverter.insertDirToSQL(dir, conn)
		return result

	def runToObj(self, input_file, parameters, dir):
		result = self.run(input_file, parameters, dir)
		return NSToSQLConverter.dirToObj(dir)

class NSLibError(Exception):
	"""Exception raised when various error related to this module happens"""
	def __init__(self, error):
		self.error = error
	def __str__(self):
		return repr(self.error)

# Converter class definition for other classes to extend from
class NSToSQLConverter(object):
	"""Converter class for other converters to extend from.
	An example usage can be seen from this file
	"""
	__metaclass__ = ABCMeta

	# Method that returns table create statement
	# for the file type it supports
	@abstractmethod
	def createSQL(self): pass

	# Method that returns insert statement
	# for the file type it supports
	@abstractmethod
	def insertSQL(self): pass

	# Method that returns table entries from file
	# Extending classes can just use parents method
	# if only thing required is to split each line
	@abstractmethod
	def entries(self, file):
		entryarr = []
		for line in file:
			entryarr.append(line.strip().split())
		return entryarr

	# Method that returns which extensions a subclass supports
	@abstractmethod
	def supportedExtension(self): pass

	# Method that gets data from extending classes
	# and inserts into conn
	def insertToSQLite(self, file, conn):
		c = conn.cursor()
		c.execute(self.createSQL())
		c.executemany(self.insertSQL(), self.entries(file))
		conn.commit()

	# Method for inserting a dir into database
	# Files with an extension that does not have any supporting converter are skipped
	@staticmethod
	def insertDirToSQL(dir, conn):
		for fpath in glob.iglob(dir + "/*"):
			fname, fext = os.path.splitext(fpath)
			for child in NSToSQLConverter.__subclasses__():
				child_instance = child()
				if child_instance.supportedExtension() == fext:
					child_instance.insertToSQLite(open(fpath), conn)

	# Method for converting a dir into python object
	# Files with an extension that does not have any supporting converter are skipped
	@staticmethod
	def dirToObj(dir):
		results = {}
		for fpath in glob.iglob(dir + "/*"):
			fname, fext = os.path.splitext(fpath)
			for child in NSToSQLConverter.__subclasses__():
				child_instance = child()
				if child_instance.supportedExtension() == fext:
					results[fext] = child_instance.entries(open(fpath))
		return results

class QueueToSQLConverter(NSToSQLConverter):

	def createSQL(self):
		return """
			CREATE TABLE IF NOT EXISTS queue(
				time REAL,
				src INTEGER,
				dst INTEGER,
				q_len REAL,
				q_packet_no REAL,
				q_packet_arr INTEGER,
				q_packet_sent INTEGER,
				q_packet_drop INTEGER,
				q_data_arr INTEGER,
				q_data_sent INTEGER,
				q_data_drop INTEGER
			);
		"""

	def insertSQL(self):
		return """
			INSERT INTO queue VALUES(?,?,?,?,?,?,?,?,?,?,?)
			"""

	def entries(self, file): 
		return super(QueueToSQLConverter, self).entries(file)

	def supportedExtension(self):
		return ".queue"

class TCPToSQLConverter(NSToSQLConverter):

	def createSQL(self):
		return """
			CREATE TABLE IF NOT EXISTS tcp(
				time REAL,
				saddr INTEGER,
				sport INTEGER,
				daddr INTEGER,
				dport INTEGER,
				maxseq INTEGER,
				hiack INTEGER,
				seqno INTEGER,
				cwnd REAL,
				ssthresh INTEGER,
				dupacks INTEGER,
				rtt REAL,
				srtt REAL,
				rttvar REAL,
				bkoff INTEGER
			);
		"""

	def insertSQL(self):
		return """
			INSERT INTO tcp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
			"""

	def entries(self, file): 
		entryarr = []
		for line in file:
			entryarr.append(line.strip().split()[1::2])
		return entryarr
	
	def supportedExtension(self):
		return ".tcp"


class TRToSQLConverter(NSToSQLConverter):

	def createSQL(self):
		return """
			CREATE TABLE IF NOT EXISTS tr(
				event_name TEXT,
				time REAL,
				src INTEGER,
				dst INTEGER,
				packet_type TEXT,
				packet_size INTEGER,
				flags TEXT,
				flow_number INTEGER,
				packet_sender TEXT,
				packet_receiver TEXT,
				seq_no INTEGER,
				packet_id INTEGER
			);
		"""

	def insertSQL(self):
		return """
			INSERT INTO tr VALUES(?,?,?,?,?,?,?,?,?,?,?,?)
			"""

	def entries(self, file): 
		return super(TRToSQLConverter, self).entries(file)
	
	def supportedExtension(self):
		return ".tr"

class UDPToSQLConverter(NSToSQLConverter):

	def createSQL(self):
		return """
			CREATE TABLE IF NOT EXISTS udp(
				time REAL,
				traffic_rate REAL,
				bandwith REAL
			);
		"""

	def insertSQL(self):
		return """
			INSERT INTO udp VALUES(?,?,?)
			"""

	def entries(self, file): 
		return super(UDPToSQLConverter, self).entries(file)
	
	def supportedExtension(self):
		return ".udp"



# For testing when run from command line directly
def main(argv):
	nslib = NSLib("ns")


if __name__ == "__main__":
	main(sys.argv[1:])

	