##### kadai2.tcl #####
#
# This script is used for problem 2.
#  �� Since TCP trace isnot used in problem 2, comment out these scripts
#  ��  Since it is no need data for Nam, comment out these scripts
# ����using comment out, the simulation time is decreased
#
# Topology,in () is name of Agent
#
#   (tcp)                          (sink)
#     n0                             n4
#       \                           /
#    5ms \                         / 5ms
#  25Mbps \                       / 25Mbps
#          \          bw         /
#          n2-------------------n3
#          /         d          \    
#  25Mbps /                       \ 25Mbps
#    5ms /                         \ 5ms
#       /                           \
#     n1                             n5
#   (tcp)                          (sink) 
#
# (Notice) It is possible to set bandwidth of link, CBR rate to Kb,Mb,Gb.
#       Kb-->Kbps, 1Kb=1000bps
#       Mb-->Mbps, 1Mb=1000Kb
#       Gb-->Gbps, 1Gb=1000Mb
#
# (Notice) In this simulation 1[kbytes]=1000[bytes]
#

##### Input parameter
# input bw and d for simulation. if bw=10Mbps and d=20ms, use command {ns kadai2-1.tcl 10Mb 20ms}

# bandwidth of bottleneck link 10Mb
set bw "[lindex $argv 0]"
# delay in bottleneck 20ms
set d "[lindex $argv 1]"
# queue size of bottleneck 32
set qs "[lindex $argv 2]"
# packet size 1000
set ps "[lindex $argv 3]"
# TCP max window size 64
set ws "[lindex $argv 4]"


##### Declare Simulator 
set ns [new Simulator]

##### Setting output file
set file [open out.tr w]
$ns trace-all $file
set namfile [open out.nam w]
$ns namtrace-all $namfile
set tcpfile [open out.tcp w]
Agent/TCP set trace_all_oneline_ true

##### Setting Queue(donot modify for all case)
Queue/DropTail set queue_in_bytes_ true
Queue/DropTail set mean_pktsize_ 1000

##### Setting Node
set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]

##### Setting Link
$ns duplex-link $n0 $n2 25Mb  5ms DropTail
$ns duplex-link $n1 $n2 25Mb  5ms DropTail
$ns duplex-link $n2 $n3 $bw $d DropTail  ;#bottleneck link
$ns duplex-link $n3 $n4 25Mb  5ms DropTail
$ns duplex-link $n3 $n5 25Mb  5ms DropTail
$ns duplex-link-op $n0 $n2 orient right-down
$ns duplex-link-op $n1 $n2 orient right-up
$ns duplex-link-op $n2 $n3 orient right
$ns duplex-link-op $n3 $n4 orient right-up
$ns duplex-link-op $n3 $n5 orient right-down
$ns duplex-link-op $n2 $n3 queuePos 0.5

##### Setting queue size
$ns queue-limit $n2 $n3 $qs  ;#bottleneck queue size[kbytes]

##### setting TCP Agent
### setting TCP Agent parameter
Agent/TCP set packetSize_ $ps ;#TCP packet size(not include 40[bytes] header)[bytes]
Agent/TCP set window_ $ws       ;#TCP maximum window size[packets]
### setting TCP Agent
### Flow1
#set tcp [new Agent/TCP]
set tcp [new Agent/TCP/Reno]
#set tcp [new Agent/TCP/Newreno]
$ns attach-agent $n0 $tcp
set sink [new Agent/TCPSink]
$ns attach-agent $n4 $sink
$ns connect $tcp $sink
$tcp set fid_ 0
$ns color 0 blue
### setting output of TCP Agent
$tcp attach-trace $tcpfile
$tcp trace cwnd_

##### setting FTP Application 
set ftp [new Application/FTP]
$ftp attach-agent $tcp

##### Setting time schedule of simulation
$ns at  0.0 "$ftp start"
$ns at 20.0 "$ftp stop"
$ns at 20.0 "finish"
proc finish {} {
    global ns file namfile tcpfile
    $ns flush-trace
    close $file
    close $namfile
    close $tcpfile
    exit 0
}

#####  Finish setting and start simulation
$ns run
